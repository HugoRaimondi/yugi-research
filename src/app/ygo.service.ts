import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class YgoService {

  private baseUrl = 'https://db.ygoprodeck.com/api/v7/cardinfo.php?'
  constructor(private http: HTTP) { }

  findByName(name: string) {
    return this.http.get(this.baseUrl + "name=" + name,{},{});
  }
}
