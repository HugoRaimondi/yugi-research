import { TestBed } from '@angular/core/testing';

import { YgoService } from './ygo.service';

describe('YgoService', () => {
  let service: YgoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(YgoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
