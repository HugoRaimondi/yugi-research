import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';

import { Camera } from '@ionic-native/camera/ngx';

export const firebaseConfig = {
  apiKey: "AIzaSyB3NKlgWwZRZG2wuRU4L2NC8dOi7hUkV7E",
  authDomain: "yugi-research.firebaseapp.com",
  databaseURL: "https://yugi-research.firebaseio.com",
  projectId: "yugi-research",
  storageBucket: "yugi-research.appspot.com",
  messagingSenderId: "724800531417",
  appId: "1:724800531417:web:0b1e909c2b37b3e0e347c8",
  measurementId: "G-X5BKXML4SS"
};

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
