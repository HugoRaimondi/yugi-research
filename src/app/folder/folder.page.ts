import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { YgoService } from '../ygo.service';

@Component({
  selector: 'app-folder',
  templateUrl: './folder.page.html',
  styleUrls: ['./folder.page.scss'],
})
export class FolderPage implements OnInit {
  public folder: string;
  public inputValue: string = "";
  constructor(private activatedRoute: ActivatedRoute, public navCtrl: NavController) { }

  ngOnInit() {
    this.folder = '/folder/Inbox';
  }

  test(){
    console.log(this.inputValue);
    //this.ygoService.findByName(this.inputValue);
  }

}
